package com.example.michael.practicaaa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
Button botonSensor, botonVibracion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonSensor=(Button) findViewById(R.id.btnSensores);
        botonVibracion = (Button) findViewById(R.id.btnVibracion);

        botonSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        botonVibracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadVibrar.class);
                startActivity(intent);
            }
            });
    }

}
